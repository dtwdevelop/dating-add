<?php

use yii\db\Schema;
use yii\db\Migration;

class m150326_224055_config extends Migration
{
    public function up()
    {
       $this->createTable('config', [
            'id' => Schema::TYPE_PK,
            'category_limit' => Schema::TYPE_INTEGER,
            'page_limit' => Schema::TYPE_INTEGER,
            'foto_limit'=>  Schema::TYPE_INTEGER,
            'site_label'=>  Schema::TYPE_STRING . ' NOT NULL'
           
      ]);
    }

    public function down()
    {
        echo "m150326_224055_config cannot be reverted.\n";
          $this->dropTable('config');
        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
