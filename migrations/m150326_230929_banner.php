<?php

use yii\db\Schema;
use yii\db\Migration;

class m150326_230929_banner extends Migration
{
    public function up()
    {
       
       $this->createTable('banner', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'banner' => Schema::TYPE_INTEGER,
            'pos' => Schema::TYPE_INTEGER,
            'url' => Schema::TYPE_STRING . ' NOT NULL',
            'size_w'=>  Schema::TYPE_INTEGER,
            'site_label'=>  Schema::TYPE_STRING,
            'hide'=>  Schema::TYPE_BOOLEAN,
           
           
      ]);
    }

    public function down()
    {
        echo "m150326_230929_banner cannot be reverted.\n";
          $this->dropTable('banner');
        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
