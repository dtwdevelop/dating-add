<?php

use yii\db\Schema;
use yii\db\Migration;

class m150326_224022_foto extends Migration
{
    public function up()
    {
        $this->createTable('foto', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'add_id'=>Schema::TYPE_STRING . ' NOT NULL',
            'user_id' => Schema::TYPE_STRING . ' NOT NULL',
            'small_foto'=> Schema::TYPE_STRING . ' NOT NULL',
            'big_foto'=> Schema::TYPE_STRING . ' NOT NULL',
            'created_at'=>  Schema::TYPE_DATETIME,
      ]);
    }

    public function down()
    {
        echo "m150326_224022_foto cannot be reverted.\n";
          $this->dropTable('foto');
        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
