<?php

use yii\db\Schema;
use yii\db\Migration;

class m150326_223937_category extends Migration
{
    public function up()
    {
      $this->createTable('category', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
             'pos'=>  Schema::TYPE_INTEGER,
             'hide'=>  Schema::TYPE_BOOLEAN,
             'created_at'=>  Schema::TYPE_DATETIME,
      ]);
    }

    public function down()
    {
        echo "m150326_223937_category cannot be reverted.\n";
          $this->dropTable('category');
        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
