<?php

use yii\db\Schema;
use yii\db\Migration;

class m150326_223957_add extends Migration
{
    public function up()
    {
        $this->createTable('add', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'login' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'phone' => Schema::TYPE_STRING . ' NOT NULL',
            'age' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_STRING . ' NOT NULL',
            'choise'=>Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'ip' => Schema::TYPE_TEXT,
            'pos'=>  Schema::TYPE_INTEGER,
            'hide'=>  Schema::TYPE_BOOLEAN,
            'ban'=>  Schema::TYPE_BOOLEAN,
            'created_at'=>  Schema::TYPE_DATETIME,
      ]);
    }

    public function down()
    {
        echo "m150326_223957_add cannot be reverted.\n";
          $this->dropTable('add');

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
