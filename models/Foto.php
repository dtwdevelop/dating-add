<?php

namespace app\models;

use Yii;
use app\models\Add;
use app\models\AddSite;
use yii\db\Expression;

/**
 * This is the model class for table "foto".
 *
 * @property integer $id
 * @property string $title
 * @property string $add_id
 * @property string $user_id
 * @property string $small_foto
 * @property string $big_foto
 * @property string $created_at
 */
class Foto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foto';
    }
    
     public function getAdd(){
        return $this->hasOne(Add::className(), ['id'=>'add_id']);
    }
    
     public function getSite(){
        return $this->hasOne(AddSite::className(), ['id'=>'add_id']);
    }
    
     public function beforeSave($insert) {
        if($this->isNewRecord){
        
        $this->created_at = new Expression('NOW()');
        }
       return parent::beforeSave($insert);
       
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'add_id', 'user_id', 'small_foto', 'big_foto'], 'required'],
            [['created_at'], 'safe'],
            [['title', 'add_id', 'user_id', 'small_foto', 'big_foto'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'add_id' => Yii::t('app', 'Add ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'small_foto' => Yii::t('app', 'Small Foto'),
            'big_foto' => Yii::t('app', 'Big Foto'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
