<?php

namespace app\models;
use yii\base\Model;
class Files extends Model {
   
    public $fotos;
    
    
      public function rules()
    {
        return [
            [['fotos'], 'safe'],
            [['fotos'], 'file', 'maxFiles' => 3,'extensions'=>'jpg, gif, png'],
           
           
        ];
    }
    
     public function attributeLabels()
    {
        return [
            'fotos' => 'Files',
            
        ];
    }
}
