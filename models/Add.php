<?php

namespace app\models;

use Yii;
use app\models\Category;
use yii\helpers\ArrayHelper;
use app\models\Foto;
use yii\db\Expression;
/**
 * This is the model class for table "add".
 *
 * @property integer $id
 * @property string $title
 * @property string $login
 * @property string $email
 * @property string $phone
 * @property integer $age
 * @property string $user_id
 * @property string $choise
 * @property string $content
 * @property string $ip
 * @property integer $pos
 * @property integer $hide
 * @property integer $ban
 * @property string $created_at
 */
class Add extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public $choices =['1'=>'Dating','2'=>'Party','3'=>'Sex'];
    public static function tableName()
    {
        return 'add';
    }
    
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'user_id']);
    }
     public function getFotos(){
        return $this->hasMany(Foto::className(), ['add_id'=>'id']);
    }
    
      public function beforeSave($insert) {
        if($this->isNewRecord){
        $this->pos=1;
        $this->hide=0;
        $this->ban=0;
        $this->ip =base64_encode(Yii::$app->getSecurity()->generateRandomString());
        $this->created_at= new Expression('NOW()');
        }
       return parent::beforeSave($insert);
       
    }
    
     public function getAges(){
         
       $ages= range(18,100) ;
      
       
       return $ages;
    }
    
      public function getChoise(){
       return $this->choices;
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'login', 'email', 'user_id', 'choise','phone','age'], 'required'],
            [['content', 'ip','phone'], 'string'],
            [['pos', 'hide', 'ban','age'], 'integer'],
            [['created_at','file'], 'safe'],
             ["email",'email'],
            [['file'], 'file', 'maxFiles' => 3,'extensions'=>'jpg, gif, png'],
            
            [['title', 'login',  'user_id', 'choise'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'login' => Yii::t('app', 'Login'),
            'email' => Yii::t('app', 'Email'),
            'user_id' => Yii::t('app', 'User ID'),
            'choise' => Yii::t('app', 'Choise'),
            'content' => Yii::t('app', 'Short'),
            'ip' => Yii::t('app', 'All'),
            'pos' => Yii::t('app', 'Pos'),
            'hide' => Yii::t('app', 'Hide'),
            'ban' => Yii::t('app', 'Ban'),
            'created_at' => Yii::t('app', 'Created At'),
             'fotos' => 'Files',
            
        ];
    }
}
