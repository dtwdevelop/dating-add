<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Add;
/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $pos
 * @property integer $hide
 * @property string $created_at
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }
    public function getAdds(){
      return  $this->hasMany(Add::className(),['user_id'=>'id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['content'], 'string'],
            [['pos', 'hide'], 'integer'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 255]
        ];
    }
    
    public static function Listcategory($id=0){
      if($id ==0){
      $root = array('0'=>'Root');
      }
      $data = self::find()->all();
      
      $arr = ArrayHelper::toArray($data);
      $arr = ArrayHelper::map($arr, 'id','title');
      if($id >0){
          
      }return $arr;
      return ArrayHelper::merge($root, $arr);
              
    }
    
    public function getAddActive(){
       $adds =$this->adds;
       $active = array();
       foreach($adds as $k=> $val){
           if($val->hide == 1){
             $active[$k] =$val;
           }
       }
      return count($active);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'content' => Yii::t('app', 'Content'),
            'pos' => Yii::t('app', 'Pos'),
            'hide' => Yii::t('app', 'Hide'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
