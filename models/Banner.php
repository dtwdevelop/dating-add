<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $title
 * @property integer $banner
 * @property integer $pos
 * @property string $url
 * @property integer $size_w
 * @property string $site_label
 * @property integer $hide
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['banner', 'pos', 'size_w',  'hide'], 'integer'],
            [['title','site_label', 'url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'banner' => Yii::t('app', 'Banner'),
            'pos' => Yii::t('app', 'Pos'),
            'url' => Yii::t('app', 'Url'),
            'size_w' => Yii::t('app', 'Size W'),
            'site_label' => Yii::t('app', 'Site Label'),
            'hide' => Yii::t('app', 'Hide'),
        ];
    }
}
