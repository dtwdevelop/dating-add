<?php

namespace app\models;
use yii\db\Expression;
use Yii;
use app\models\Category;
use yii\helpers\ArrayHelper;
use app\models\Foto;
/**
 * This is the model class for table "add".
 *
 * @property integer $id
 * @property string $title
 * @property string $login
 * @property string $email
 * @property string $phone
 * @property integer $age
 * @property string $user_id
 * @property string $choise
 * @property string $content
 * @property string $ip
 * @property integer $pos
 * @property integer $hide
 * @property integer $ban
 * @property string $created_at
 */
class AddSite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public $verifyCode;
    public $choices =['1'=>'Dating','2'=>'Party','3'=>'Sex'];
    public $posted = ['1'=>'One week','2'=>'Two week','3'=>'One Month'];
    public $key;
//    public $checkcap= false;
    public static function tableName()
    {
        return 'add';
    }
    public function getChoise(){
       return $this->choices;
    }
    
     public function getTimePeriod(){
       return $this->posted;
    }
    
     public function getAges(){
         $ages= array();
         $data =range(18,100);
         foreach ($data as $age){
             $ages[$age]=$age;
         }
       return $ages ;
    }
    
    public function find_active_add($show){
       $total = $this->find()->where(['hide'=>$show]);
       return count($total);
    }
    
     public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'user_id']);
    }
    
      public function getFotos(){
        return $this->hasMany(Foto::className(), ['add_id'=>'id']);
    }

    public function beforeValidate() {
        if($this->isNewRecord){
        $this->pos=1;
        $this->hide=0;
        $this->ban=0;
        $this->ip = Yii::$app->getSecurity()->generatePasswordHash($this->key);
        $this->created_at= new Expression('NOW()');
        }
       return parent::beforeValidate();
       
    }
    
   
    
    public function sendEmail(){
        $body="<p>Thanks you, To active you ad click link</p> <a href=".Yii::$app->params['adDelete']."?token=".base64_encode($this->id).">Active now </a>";
             Yii::$app->mailer->compose()
                ->setTo($this->email)
                ->setFrom(Yii::$app->params['adminEmail'])
                ->setSubject("To Active you ad please click link")
                ->setHtmlBody($body)
                ->send();
        }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'login', 'email', 'user_id', 'choise','age','key','pos'], 'required'],
            [['content', 'ip'], 'string'],
            [['pos', 'hide', 'ban','age'], 'integer'],
             ['phone', 'match', 'pattern' => '/(\+\d)*\s*(\(\d{3}\)\s*)*\d{3}(-{0,1}|\s{0,1})\d{2}(-{0,1}|\s{0,1})\d{2}/i'],
            ["email",'email'],
            [['created_at','file'], 'safe'],
             [['file'], 'file', 'maxFiles' => 3,'extensions'=>'jpg, gif, png'],
//             ['verifyCode', 'captcha'],
            [['title', 'login', 'email', 'user_id', 'choise'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'login' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'user_id' => Yii::t('app', 'Category'),
            'choise' => Yii::t('app', 'Choise Type'),
            'content' => Yii::t('app', 'Text'),
            'ip' => Yii::t('app', 'Hash you'),
            'pos' => Yii::t('app', 'Posted times'),
            'hide' => Yii::t('app', 'Hide'),
            'ban' => Yii::t('app', 'Ban'),
             'key' => Yii::t('app', 'Password'),
            'created_at' => Yii::t('app', 'Created At'),
           
             'verifyCode' => 'Verification Code',
              'fotos' => 'Files',
        ];
    }
}
