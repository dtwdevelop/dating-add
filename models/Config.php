<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property integer $id
 * @property integer $category_limit
 * @property integer $page_limit
 * @property integer $foto_limit
 * @property integer $site_label
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_limit', 'page_limit', 'foto_limit'], 'integer'],
             [['site_label'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_limit' => Yii::t('app', 'Category Limit'),
            'page_limit' => Yii::t('app', 'Page Limit'),
            'foto_limit' => Yii::t('app', 'Foto Limit'),
            'site_label' => Yii::t('app', 'Site Label'),
        ];
    }
}
