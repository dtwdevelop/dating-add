<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Category;
use app\models\AddSite;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use app\models\Foto;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\captcha\CaptchaValidator;
use yii\db\Query;
use yii\data\Sort;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->redirect(['adds', 'id' => 1]);
    }
    
    public function actionCategory(){
     $provider = new ActiveDataProvider([
    'query' => Category::find(),
    'pagination' => [
        'pageSize' => 10,
    ],
]);
     return $this->render('category',['provider'=>$provider]);
    
    }
    
      public function actionAdds(){
     $id = Yii::$app->request->get('id');
//     if($id == null){
//         throw new NotFoundHttpException('No adds for category');
//     }
      $sort = new Sort([
        'attributes' => [
            
            'age' => [
                'asc' => ['age' => SORT_ASC],
                'desc' => ['age' => SORT_DESC],
               
                'label' => Yii::t('app', 'by Age'),
            ],
            'created_at' => [
                'asc' => ['created_at' => SORT_ASC],
                'desc' => ['created_at' => SORT_DESC],
               
                'label' => Yii::t('app', 'by Date'),
            ],
           
        ],
    ]);
      $add = new AddSite();
     $provider = new ActiveDataProvider([
    'query' => AddSite::find()->where(['user_id'=>$id,'hide'=>1])->orderBy($sort->orders),
          'sort' => [
        // Set the default sort by name ASC and created_at DESC.
        'defaultOrder' => [
           
            'created_at' => SORT_DESC
        ]
    ],
    'pagination' => [
        'pageSize' => 10,
    ],
]);
     return $this->render('adds',['provider'=>$provider,'model'=>$add, 'sort'=>$sort,]);
    
    }
    
       public function actionPage(){
     $id = Yii::$app->request->get('id');
     
     $request = Yii::$app->request;
     if($request->isAjax){
          $id = Yii::$app->request->post('id');
         if(!$adds =AddSite::findOne(['id'=>$id])){
         throw new NotFoundHttpException('No Page for Adds');
     }
        
         $imagecheck =Yii::$app->request->post('imagecheck');
         $validator  = new CaptchaValidator();
         if($validator->validate($imagecheck,$error)){
            $check=true;
            return $this->renderPartial('add',['adds'=>$adds,'check'=>$check,'err'=>'']);
         }
         else{
             $check=false;
             return $this->renderPartial('add',['adds'=>$adds,'check'=>$check,'err'=>$error]);
         }
         
       }
     
     
     if(!$adds =AddSite::findOne(['id'=>$id])){
         throw new NotFoundHttpException('No Page for Adds');
     }
     else{
        
       
       return $this->render('add',['adds'=>$adds,'check'=>false,'err'=>'']);
     }
    
    }
     public function actionCreate()
    {
        $model = new AddSite();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->sendEmail();
               $files= $_POST['AddSite']['file'];
             $model->file =$files;
           //   var_dump($files);
           $files = UploadedFile::getInstances($model, 'file');
           
           $id =$model->id;
           foreach ($files as $f){
               $foto  = new Foto();   
//            var_dump($f->baseName);
           $file =Yii::$app->getSecurity()->generateRandomString().$f->baseName;  
           $foto->title ='foto';
           $foto->add_id= "$id";
           $foto->user_id='1';
           $foto->small_foto='s_'.$file .'.jpeg' ;
           $foto->big_foto='b_'.$file .'.jpeg' ;
          
           $f->saveAs(Yii::$app->params['big'] . $foto->big_foto);
           Image::thumbnail(Yii::$app->params['big'] .$foto->big_foto, 200, 200)->save(Yii::$app->params['small'].$foto->small_foto);
            if($foto->validate()){
                $foto->save();
            }
            else{
                var_dump($foto->errors);
            }
           
           }
             Yii::$app->session->setFlash('AddCreate');
             return $this->redirect(['page', 'id' => $model->id]);
            
        } else {
            
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDelete(){
        $request = Yii::$app->request;
       if($request->isPost){
      $id = Yii::$app->request->post('val');
      $key = Yii::$app->request->post('key');
      if(!$model = AddSite::findOne(['id'=>$id])){
          Yii::$app->session->setFlash('Error mail');
          return  $this->renderPartial('delete');
      }
      else{
          
           if(Yii::$app->getSecurity()->validatePassword($key,$model->ip) ){
                 Yii::$app->session->setFlash('Addelete');
            Foto::deleteAll(['add_id'=>$model->id]);
          $model->delete();
           echo "all ok $model->id";
            Yii::$app->session->setFlash('DeleteAd');
           return $this->redirect(['category']);
             
               
             
           }
           else{
                  Yii::$app->session->setFlash('Password Error');
              return   $this->renderPartial('delete',['model'=>$model]);
           }

      }
         }
         else{
              return $this->redirect(['category']);
         }
    }

    public function actionActive(){
        $token = Yii::$app->request->get('token');
      if(!$model = AddSite::findOne(['id'=>  base64_decode($token)])){
          throw new NotFoundHttpException('Invalid token Token');
      }
      else{
          
          $model->hide=1;
          
          $model->save(false);
         
            Yii::$app->session->setFlash('Active');
             return $this->redirect(['category']);
      }
    }
   
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
    public function actionInfo()
    {
        return $this->render('info');
    }
}
