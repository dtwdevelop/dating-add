<?php

namespace app\controllers;

use Yii;
use app\models\Add;
use app\models\AddSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\Foto;
use yii\web\UploadedFile;
use yii\imagine\Image;

/**
 * AddController implements the CRUD actions for Add model.
 */
class AddController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
              'class' => AccessControl::className(),
              'only' => ['index','create','update','delete','view'],
              'rules' => [

                [
                       'allow' => true,
                       'actions' => ['index','view'],
                       'roles' => ['?'],

                   ],
                  [

                      'allow' => true,
                      'actions' => ['index','create','update','delete','view','page'],
                      'roles' => ['@'],
                  ],
              ],
          ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Add models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AddSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Add model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Add model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Add();
        
         
           
         if ($model->load(Yii::$app->request->post()) && $model->save()) {
             $files= $_POST['Add']['file'];
             $model->file =$files;
           //   var_dump($files);
           $files = UploadedFile::getInstances($model, 'file');
            var_dump($files);
           $id =$model->id;
           foreach ($files as $f){
           $foto  = new Foto();
           $file =Yii::$app->getSecurity()->generateRandomString().$f->baseName;  
           $foto->title ='foto';
           $foto->add_id= "$id";
           $foto->user_id='1';
           $foto->small_foto='s_'.$file .'.jpeg' ;
           $foto->big_foto='b_'.$file .'.jpeg' ;
          
           $f->saveAs(Yii::$app->params['big'] . $foto->big_foto);
           Image::thumbnail(Yii::$app->params['big'] .$foto->big_foto, 200, 200)->save(Yii::$app->params['small'].$foto->small_foto);
            if($foto->validate()){
                $foto->save();
            }
            else{
                var_dump($foto->errors);
            }
           
           }
             return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
               
            ]);
        }
    }

    /**
     * Updates an existing Add model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
          $foto  = new Foto();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'foto'=>$foto,
            ]);
        }
    }

    /**
     * Deletes an existing Add model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Add model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Add the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Add::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
