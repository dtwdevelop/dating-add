<?php

namespace app\controllers;

use app\models\AddSite;
use ZendSearch\Lucene\Lucene;
use ZendSearch\Lucene\Document;
use ZendSearch\Lucene\Document\Field;
use ZendSearch\Lucene\Search\QueryParser;
use Yii;


class SearchController extends \yii\web\Controller {

    public function actionIndexx(){
      $search = new SearchForm;
      return $this->render('search', ['model' => $search,'rezult' =>null]);

    }
    public function actionData() {
        $req = Yii::$app->request;
        

        if ($q=Yii::$app->request->get('q')) {
            $this->Create();
            $find = $q;
            $query = QueryParser::parse($find, 'UTF-8');
            $index = Lucene::open(Yii::$app->params['index']);
            $hits = $index->find($query);


            return $this->render('search', [ 'rezult' => $hits]);
        } else {
            
            return $this->render('search', ['rezult' =>null]);
        }
    }
    
 public function beforeAction($action) {
         $cookies = Yii::$app->request->cookies;
         if($cookies->has('language')){
        \Yii::$app->language = $cookies['language']->value;
         }
       return  parent::beforeAction($action);
    }
    public function Create($type=1) {
    $index = Lucene::create(Yii::$app->params['index']);
        switch ($type){
            case 1:{
             $model = AddSite::find()->all();
                if ($model !== null) {
                foreach ($model as $val) {
                    $document = new Document();
                    $document->addField(Field::unIndexed('add_id', $val->id));
                    $document->addField(Field::text('title', $val->title));
                    $document->addField(Field::text('text', $val->content));
                    
                    $document->addField(Field::unStored('date', $val->created_at));

                    $index->addDocument($document);
                }
        }
                break;
            }
         
        
        default :{
              $model = AddSite::find()->all();
                if ($model !== null) {
                foreach ($model as $val) {
                    $document = new Document();
                    $document->addField(Field::unIndexed('add_id', $val->id));
                    $document->addField(Field::text('title', $val->title));
                    $document->addField(Field::text('text', $val->content));
                   
                    $document->addField(Field::unStored('date', $val->created_at));

                    $index->addDocument($document);
                }
        }
                break;
            
        }
     }
        $index->optimize();
        $index->commit();
        $total = $index->numDocs();
    }

}
