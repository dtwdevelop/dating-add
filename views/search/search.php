<?php
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<?php Pjax::begin([]); ?>

<h3 class="well"><?= \Yii::t('app', 'Found') ?> (<?= count($rezult) ?>)</h3>
<?php if($rezult !== null): ?>

 <?php foreach ($rezult as $data): ?>

 <div class="panel panel-success">
  <div class="panel-heading panel-bg">
        
      <h3 class="panel-title "> 
        <span class="glyphicon glyphicon-heart" aria-hidden="true"></span> <?= Html::encode($data->title); ?>
      </h3>
     
  </div>
  <div class="panel-body">
   <?=  $data->text; ?>
     
     
      <div class="clearfix">
            <hr>
             <a class="btn-primary btn btn-sm pull-left "  href="<?= Url::toRoute(['/site/page','id'=>$data->add_id]) ?>">More</a>
       
      </div>
     
  </div>
</div>
<?php endforeach; ?>

<?php elseif (count($rezult)==0):?>
<h3>Not Found</h3>
<?php endif; ?>
<?php Pjax::end() ?>

