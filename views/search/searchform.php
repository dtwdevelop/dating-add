<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\jui\AutoComplete;
use app\models\Arcticle;
/* @var $this yii\web\View */
?>
<?php
 
$this->registerJs(
   '$("document").ready(function(){ 
        $("#sform").on("pjax:end", function() {
            $.pjax.reload({container:"#found"}); 
        });
    });'
);
?>
<?php Pjax::begin(['id' => 'sform']); ?>
<div class="site-login well ">
    
 <?php $form = ActiveForm::begin([
        'id' => 'login-form',
         'method'=>'get',
          'action'=>['/search/data'],
        'options' => ['class' => 'form-horizontal','data-pjax'=>false],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'q')->widget(AutoComplete::classname(), [
    'clientOptions' => [
        'source' => Arcticle::FindAllTittle(),
    ],
])  ?>

    <?= $form->field($model, 'type')->hiddenInput()->label(false); ?>
<div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton(\Yii::t('app', 'Search'), ['class' => 'btn btn-primary', 'name' => 'yes']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    </div>


   <?php Pjax::end(); ?> 

 