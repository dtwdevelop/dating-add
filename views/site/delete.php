<?php
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\Pjax;
?>
  
<?php if(\Yii::$app->session->hasFlash('Addelete')): ?>
<h3 class="alert-danger">Ad deleted !</h3>
<?php endif; ?>
<?php if(\Yii::$app->session->hasFlash('Password Error')): ?>
<h3 class="alert alert-warning">You password wrong !</h3>
<?php endif; ?>
<?php if(\Yii::$app->session->hasFlash('Error mail')): ?>
<h3 class="alert alert-warning">Can't found you id !</h3>
<?php endif; ?>
<div class="row">
<div class="col-md-6">
   
 
     <form  data-pjax="1" method="post" action="/site/delete">
     <div class="form-group">
   
    <input type="hidden" class="form-control" id="exampleInputEmail1" value="<?= $model->id ?>" name="val" placeholder="Enter email">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input required type="password" class="form-control" name="key" id="exampleInputPassword1" placeholder="Password">
  </div>
    <div class="form-group">
   
        <input type="submit" value="Delete" class="btn btn-danger">
  </div>
 </form>
    
    
    
</div>
</div>
 


