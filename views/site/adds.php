<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\LinkSorter;
?>


<?php Modal::begin([
    'id'=>'modal',
    'header' => '<h3>Add new</h3>',
    'toggleButton' => ['label' => '<span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>  Create Ad','class'=>'btn btn-primary'],
    'size'=>'modal-lg'
]); ?>

<div >
     <?php echo $this->render('/site/create', ['model' => $model]) ;?>


</div> 
  
<?php Modal::end();?>



<?php if(isset($sort)){ echo LinkSorter::widget(['sort'=>$sort,'options' => ['class' => 'btn-group  nav nav-pills','style'=>'margin:5px'],]); }?> 
<?php



 echo ListView::widget([
    'dataProvider' => $provider,
    'itemView' => 'partial/_adds',
]);
?>


