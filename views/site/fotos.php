<?php 
use yii\helpers\Url;
use yii\helpers\Html;
?>
<br>
<?php  if($adds->fotos) :?>
      <div>
    <?php foreach ($adds->fotos as $foto): ?>
  
          <a class="media-left fancy"  data-pjax="0"  rel="group"  href="<?= Url::to(['/upload/big/'.str_replace('\\', '/', $foto->big_foto)]) ?>">
    <?= Html::img('/upload/small/'.str_replace('\\', '/', $foto->small_foto), ['width'=>'100','class'=>'img-thumbnail']); ?>
          </a>    
    <?php endforeach; ?>
      </div>
  <?php endif; ?>
