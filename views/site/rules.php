<?php 
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'id'=>'modal3',
    'header' => '<h3>Rules</h3>',
    'toggleButton' => ['label' => '<span class="fa fa-info" aria-hidden="true">'
        . '</span>  Rules','class'=>'btn btn-success btn-sm'],
    'size'=>'modal-md'
]); ?>

<h2>
TERMS AND CONDITIONS</h2>
    <p>
TERMS AND CONDITIONS
Effective Date: 8 April 2015
<h3>1. Introduction</h3>
1.1 This website is Free
1.2 Please read these terms and conditions (T&Cs) carefully. They cancel and replace any previous versions. No information obtained by you from this website (or via this website) shall create any warranty not expressly stated in these Terms.
<h3>2. Definitions</h3>
2.1 Just to explain a few key terms. When we mention “Member” below, we mean a registered user of our site (whether or not a subscriber). And “Content” covers all information (including profile, images, photos, videos, forum posts, messages etc) which Members publish or send on or in connection with our site.
<h3>3. Changes to the T&Cs </h3>
3.1 We may need to change these T&Cs from time to time. If so, we will post the revised version on our website for a reasonable period before they become effective. Please check our website from time to time. You will be bound if you use the site after the effective date shown on the new T&Cs (except that if you have a current subscription when we post the new T&Cs, the effective date is postponed until your subscription expires).
<h3>4. Use of our site</h3>
4.1 You must not use or register on our site if you are under 18 years of age or if display of or access to the material contained on this site is illegal under the laws (if applicable) of the country from which you are accessing the site eg because the country does not permit display of or access to the material at all or because you are under the relevant age limit in that country. Also you mustn’t use or register on our site if you have been convicted of any offence, or subject to any court order, relating to assault, violence, sexual misconduct or harassment. (Any breach of this clause is a serious breach of this agreement.)
4.2 You must ensure that your profile and any other Content is and remains complete, accurate and is not misleading in any way and that you will update it so that it remains so.
543 You must at all times comply with our acceptable use policy as well as any guidelines or requirements on our site and any reasonable request or instructions by us in connection with the site.
4.4 Please note that our collection and use of personal data is subject to privacy policy.
4.5 You must not under any circumstances publish or send any Content which (1) involves an image of any person under 18 (or any higher applicable age of consent in countries outside the UK) or (2) enables any person under 18 to be identified or contacted (eg name, address, email etc).
4.6 In the case of those over 18 (or higher applicable age of consent), you should not publish or send any Content (including images) which enables those persons to be identified unless you have obtained their explicit written consent to the image / information being provided as well as to these T&Cs and our privacy policy.
4.7 You will understand that we have to reserve the right to vet or monitor any Content and to reject, suspend or remove from our site or to disclose to the relevant authorities any Content if it breaches our terms and conditions or is necessary to protect us or others or if we are required to do so by law or appropriate authority. If so, you must not attempt to re-publish or re-send the relevant Content.
4.8 You must notify us immediately if you become aware of any inappropriate Content or inappropriate behaviour by any user of our site. You can do this by clicking on the "Report User to Admin" link at the bottom of any profile.
4.9 You must take reasonable care to protect and keep confidential your password and other account or identity information. You must notify us immediately of any apparent breach of security such as loss, theft, misuse or unauthorised disclosure or use of a password. You are responsible for third parties who use your account or identity (unless and to the extent that we are at fault).
<h3>5. Functioning of our site</h3>
5.1 You will need reliable internet access. We cannot accept any responsibility for your hardware or software or for the costs of accessing our site. We will do our best to maintain the operation of our site and to rectify faults if they occur but cannot guarantee that the site will be uninterrupted or error-free.
5.2 We may have to suspend the site for repair, maintenance, improvement or other technical reason. If so, we will do our best to ensure that the suspension takes place at a time when our site is least likely to be used and that the suspension is for the shortest period possible.
5.3 We may make changes to the site provided that these do not have a significant adverse effect on the quality of the Site.
<h3>6. Third party websites</h3>
6.1 We may link to third party websites which may be of interest to you. We do not recommend or endorse those sites or the products or services which they offer nor are we responsible for them as they are outside our reasonable control. You use such third party sites at your own risk.
<h3>7. Intellectual property rights</h3>
7.1 All trade marks, logos, graphics, images, photographs, animation, videos, text and software used on the site (including Content) are our intellectual property or that of our partners. You may display, reproduce or otherwise use such content insofar as necessary to view it within our site for genuine, private, non-commercial purposes. You may not otherwise retrieve, display, modify, copy, print, sell, download, hire or reverse engineer (unless permitted by applicable law) or use such content without our prior written consent.
7.2 If you publish any Content, you are allowing us to copy, alter, adapt or display such Content on our site.
<h3>8. Liability</h3>
8.1 Nothing in this agreement in any way limits or excludes our liability for negligence causing death or personal injury or for fraudulent misrepresentation or for anything which may not legally be excluded or limited.
8.2 We shall not be liable for any loss or damage caused by us or our employees or agents in circumstances where: 
8.2.1 there is no breach of a legal duty of care owed to you by us or by any of our employees or agents; 
8.2.2 such loss or damage is not a reasonably foreseeable result of any such breach; 
8.2.3 such loss or damage is caused by you, for example by not complying with this agreement; or 
8.2.4 such loss or damage relates to a business.
8.2.5 You will liable for any loss or damage we suffer arising from your breach of this agreement or misuse of our site.
<h3>9. Force majeure</h3>
9.1 We are not liable for failure to perform or delay in performing any obligation under this agreement if the failure or delay is caused by any circumstances beyond our reasonable control. If our site is unavailable for more than 48 hours, we are not responsible for this  period.
10. General 
10.1 Headings used in this agreement are for information and not binding. This agreement constitutes the entire agreement between you and us in connection with our site. We may assign all or part of our rights or duties under this agreement provided we ensure that your rights under this agreement are not prejudiced. You may assign all or part of your rights under this agreement only with our prior written consent which may not be unreasonably withheld or delayed. Any failure by either party to exercise or enforce any right or provision of this agreement does not constitute a waiver of it. If any part of this agreement is deemed void or ineffective for any reason, the remainder shall continue in full force. A person who is not a party to this agreement shall have no rights under the Contracts (Rights of Third Parties) Act 1999 to enforce any term of this agreement except insofar as expressly stated otherwise.
<h3>11. Law and courts</h3>
11.1 This contract shall be governed by English law and any disputes will be decided only by the courts of the United Kingdom.
<h3>12. Complaints </h3>
12.1 If you have any complaints, please contact us via the contact details shown on our website Nsexdate.co.uk





<h3>Privacy Policy</h3>
Any private information such as email address or password will be kept private using reasonable care.
We'll never sell your email address or any private information to a 3rd party.
Any information you put on your profile is public information and will be shown on the website.
We make use of a cookie to help manage logged in users on the site. We also use 3rd party analytics software to measure how many visitors, pageviews etc that the site receives which also uses cookies.
Some of your browsing information is public on your profile, for example the date you registered and when you were last on the site.
You can remove yourself and all your data from the site at any time using the remove password


</p>



  
<?php Modal::end();?>

