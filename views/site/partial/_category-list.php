<?php
use yii\helpers\Url;
?>
<div class="row">
<div class="col-md-9 well">
    <a href="<?= Url::toRoute(['site/adds', 'id' => $model->id])?>">
      
        <h4><?=$model->title ?>
            (<?= $model->getAddActive()?>)
        </h4></a>
    <div class="text-capitalize"><?= $model->content ?></div>
    
</div>
</div>

