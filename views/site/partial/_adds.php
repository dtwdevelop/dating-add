<?php
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\HtmlPurifier;
use yii\helpers\Html;
?>
<div class="row">
<div class="col-md-8 well">
    <h3 class="">Name: <?=Html::encode($model->login) ?></h3>
    <?php if($model->fotos != null): ?>
        <?= Html::img('/upload/small/'.str_replace('\\', '/', $model->fotos[0]->small_foto), ['width'=>'80','class'=>'img-circle']); ?>
    <?php endif; ?>
    <hr>
       <div class="">
    <?php if(strlen($model->content) >128): ?>
  <?= HtmlPurifier::process(substr($model->content,0,128).'..') ?>
     <?php else: ?>
   <?= HtmlPurifier::process(substr($model->content,0,128)) ?>
     <?php endif; ?>
          
        </div>
    <?php Modal::begin([
    
   'header' => '<h2>Ad</h2>',
    'toggleButton' => ['label' => 'More info..    <span class="fa fa-external-link" aria-hidden="true"></span>','class'=>'btn btn-success'],
]); ?>

<div >
    <?=$this->render('/site/add', ['adds' => $model,'check'=>false,'err'=>'']) ;?>
</div> 
  
<?php Modal::end();?>

   
    <hr>
     <p>Age :<?= $model->age ?></p>
</div>
</div>




