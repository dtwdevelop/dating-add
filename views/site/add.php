<?php
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\helpers\Html;
use yii\captcha\Captcha;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\bootstrap\Tabs;
?>
  <?php Pjax::begin([
        
     ]);?>
<?php if(\Yii::$app->session->hasFlash('AddCreate')): ?>
<h3 class="alert alert-success">Add created but no show please active from mail!</h3>
<?php endif; ?>
<div class="row well">
   
<div class="col-md-8  ">
   
    <h3> <?=Html::encode($adds['title']) ?></h3>
    <h3>Name <?=Html::encode($adds['login']) ?></h3>
  <?php echo Tabs::widget([
    'items' => [
        [
            'label' => 'Fotos',
            'content' => $this->render('fotos',['adds'=>$adds]),
            'active' => true
        ],
        [
            'label' => 'Delete ad',
            'content' =>  $this->render('/site/delete', ['model' => $adds]),
          
          
        ],
      
    ],
]);?>
  
    <hr>
    <div class="text-capitalize margin:5px;"><?= HtmlPurifier::process($adds->content) ?></div>
     <hr>
    
    <p> age: <?= Html::encode($adds['age']) ?></p>
   
    
      <?php if($check): ?>
       <p>email: <?= Html::encode($adds['email'] )?></p>
      <?php else : ?>
       <b>To see email fill image</b>
       <form  data-pjax="1" method="post" action="/site/page">
      <p> <?= Captcha::widget([
    'name' => 'imagecheck',
    'template'=>' <p class=" col-md-6">{input}</p><div class="cols-md-3">{image}</div>'
]);
      ?>
      <p><input type="submit" value="See mail"></p>
          <input type="hidden" name="id" value="<?= $adds->id ?>">   
     
       </form>
       <span class="alert-danger"><?= $err?></span>
      <?php endif; ?>
    
      <p>phone: <?=Html::encode($adds['phone']) ?></p>
      <p>create: <?= Yii::$app->formatter->asDate($adds['created_at']) ?>   </p>
      <?php
      $type=null;
      switch ($adds['choise']){
          case 1:{
              $type ="Dating";
              break;
          }
           case 2:{
              $type ="Party";
              break;
          }
           case 3:{
              $type ="Sex";
              break;
          }
      }
      
      ?>
      <p>Want:  <?=Html::encode($type) ?>  </p>
      <p>Category: <?=Html::encode($adds->category['title']) ?></p></p>
    <a class="btn" href="<?= Url::toRoute(['adds', 'id' => $adds->category['id']])?>">Back</a>

</div>
</div>
  


<?php Pjax::end();?>



