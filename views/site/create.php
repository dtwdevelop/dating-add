<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;

use kartik\widgets\Select2;
use app\models\Category;
use yii\captcha\Captcha;
use kartik\widgets\FileInput;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Add */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Create Add');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Adds'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row ">
    <div class="col-md-9 well">
    <?php $form = ActiveForm::begin(['action'=>'/site/create','options' => [
        'enctype' => 'multipart/form-data'
        ] ]); ?>

    <?= $form->field($model, 'title',[
    'addon' => ['prepend' => ['content'=>'<i class="glyphicon glyphicon-eye-open"></i>']]
])->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'login',[
    'addon' => ['prepend' => ['content'=>'<i class="glyphicon glyphicon-user"></i>']]
])->textInput(['maxlength' => 255]) ?>
         <?= $form->field($model, 'key',[
    'addon' => ['prepend' => ['content'=>'<i class="fa fa-lock"></i>']]
])->passwordInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email',[
    'addon' => ['prepend' => ['content'=>'@']]
])->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'phone',[
    'addon' => ['prepend' => ['content'=>'<i class="glyphicon glyphicon-phone"></i>']]
])->textInput(['maxlength' => 255]) ?>
   <?= $form->field($model, 'age')->widget(Select2::classname(), [
    'language' => 'en',
       
    'data' => $model->getAges(),
    'options' => ['placeholder' => 'Select Age'],
    'pluginOptions' => [
        'allowClear' => true,
         'size'=>'SMALL',
    ],
]);?>
    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
    'language' => 'en',
       
    'data' => Category::Listcategory(1),
    'options' => ['placeholder' => 'Parent Category ...'],
    'pluginOptions' => [
        'allowClear' => true,
         'size'=>'SMALL',
    ],
]);
    ?>

    <?= $form->field($model, 'choise')->widget(Select2::classname(), [
    'language' => 'en',
       
    'data' => $model->getChoise(),
    'options' => ['placeholder' => 'Select Type ...'],
    'pluginOptions' => [
        'allowClear' => true,
         'size'=>'SMALL',
    ],
]);
    ?>
         <?= $form->field($model, 'pos')->widget(Select2::classname(), [
    'language' => 'en',
       
    'data' => $model->getTimePeriod(),
    'options' => ['placeholder' => 'Select Type ...'],
    'pluginOptions' => [
        'allowClear' => true,
         'size'=>'SMALL',
    ],
]);
    ?>
            
 <?=  $form->field($model, 'file[]',[
    'addon' => ['prepend' => ['content'=>'<i class="fa fa-file-o"></i>']]
])->widget(FileInput::classname(), [
         'options'=>['multiple'=>true],
         
         'pluginOptions' => [
         'showCaption' => true,
         'showRemove' => true,
        'showUpload' => false,
         'maxFileSize'=> '5048',
             
         
        ],
        
         
]);
 ?>
    <?= $form->field($model, 'content')->textarea(['rows' => 3,'class'=>'editor2']) ?>


   

   
 <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

