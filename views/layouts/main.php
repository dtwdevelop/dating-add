<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => '<span class="fa fa-heartbeat fa-spin fa-2x" aria-hidden="true"></span> Nsexdate',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-default navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left'],
                'items' => [
                     ['label' => 'About', 'url' => ['/site/about']],
                     ['label' => 'Post Ad', 'url' => ['/site/create']],
                     ['label' => 'Info', 'url' => ['/site/info']],
                    ['label' => 'Contact', 'url' => ['/site/contact']],
                     ['label' => 'Admin', 'visible' =>!Yii::$app->user->isGuest, 'url' => ['/site/index'],'items' => [
                         ['label' => 'Category', 'url' => ['/category/index']],
                         ['label' => 'Add', 'url' => ['/add/index']],
                          ['label' => 'Foto', 'url' => ['/foto/index']],
                         ['label' => 'Config', 'url' => ['/config/index']],
                        ]], 
                   Yii::$app->user->isGuest?
                        ['label' => ''] :
                   
                     
                        ['label' => 'Logout (' . Yii::$app->user->identity->username . ')','url' => ['/site/logout'],
                         
                    
                            'linkOptions' => ['data-method' => 'post']],
       '<li><form action="/search/data" method="get" class="navbar-form navbar-left" role="search">
        <div class="form-group ">
          <input type="text" name="q" class="form-control input-sm" placeholder="Search">
        </div>
         <button type="submit" class="btn btn-default btn-sm">Search</button>
       </form></li>'
                ],
            ]);
          
            NavBar::end();
        ?>
        
         <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-2">
          <h2></h2>
         
        </div>
        <div class="col-md-8">
          <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
       </div>
        <div class="col-md-2">
          <h2></h2>
          
        </div>
      </div>

       <!--sample-->

       
    </div>
    </div>
    <footer class="footer">
        
        <div class="container">
            <div class="row"> 
                <div class="col-md-3"><p class="pull-left" style="color:#000">&copy; Nsexdate by Centerpc <?= date('Y') ?></p></div>
             <div class="col-md-2"> <?= $this->render('/site/rules'); ?></div>
             <div class="col-md-4"> <p class="pull-right"></p></div>
            </div>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
