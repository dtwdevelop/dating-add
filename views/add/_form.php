<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\widgets\Select2;
use app\models\Category;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Add */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="add-form col-md-6">

    <?php $form = ActiveForm::begin(['options' => [
        'enctype' => 'multipart/form-data'
        ]
        ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
     <?= $form->field($model, 'phone')->textInput(['maxlength' => 255]) ?>
    
    <?= $form->field($model, 'age')->widget(Select2::classname(), [
    'language' => 'en',
       
    'data' => $model->getAges(),
    'options' => ['placeholder' => 'Select Age'],
    'pluginOptions' => [
        'allowClear' => true,
         'size'=>'SMALL',
    ],
]);?>
     <?=  $form->field($model, 'file[]')->widget(FileInput::classname(), [
         'options'=>['multiple'=>true],
         
         'pluginOptions' => [
         'showCaption' => true,
         'showRemove' => true,
        'showUpload' => false,
         'maxFileSize'=> '5048',
             
         
        ],
        
         
]);
 ?>
  
    
    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
    'language' => 'en',
       
    'data' => Category::Listcategory(),
    'options' => ['placeholder' => 'Parent Category ...'],
    'pluginOptions' => [
        'allowClear' => true,
         'size'=>'SMALL',
        
    ],
]);
    ?>

   <?= $form->field($model, 'choise')->widget(Select2::classname(), [
    'language' => 'en',
       
    'data' => $model->getChoise(),
    'options' => ['placeholder' => 'Select Type ...'],
    'pluginOptions' => [
        'allowClear' => true,
         'size'=>'SMALL',
    ],
]);
    ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 3,'class'=>'editor2']) ?>


    <?= $form->field($model, 'pos')->widget(SwitchInput::classname(), []); ?>

    <?= $form->field($model, 'hide')->widget(SwitchInput::classname(), []); ?>

    <?= $form->field($model, 'ban')->widget(SwitchInput::classname(), []); ?>

    <?= $form->field($model, 'created_at')->widget(\yii\jui\DatePicker::classname(), [
    //'language' => 'ru',
    'dateFormat' => 'yyyy-MM-dd',
]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
