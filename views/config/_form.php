<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Config */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="config-form col-md-6">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_limit')->textInput() ?>

    <?= $form->field($model, 'page_limit')->textInput() ?>

    <?= $form->field($model, 'foto_limit')->textInput() ?>

    <?= $form->field($model, 'site_label')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
